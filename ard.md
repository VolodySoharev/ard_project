# Метеостанция с возможностью сохранения данных на micro SD карту.

Данная схема метеостанции позволяет выводить информацию о температуре, давлении и высоте на экран, записывать показания на SD карту. В данной статье рассмотрено подключение датчика DH11,  вместо него можно  подключить более точный датчик BMP280, учтите что BMP280 не работает с экраном Бегущая строка из-за конликта интерфейса.

## Технические характеристики датчика DHT11
* Питание: DC 3,5 – 5,5 В;
* Ток питания:
    * в режиме измерения 0.3mA
    * в режиме ожидания 60μA
* определение влажности 20–80 % с точностью 5 %;
* определение температуры 0–50 °С с точностью 2 %;
* частота опроса не более 1 Гц (не более одного раза в 1 сек.);
* размеры 15,5´12´5,5 мм.
    
## Технические характеристики датчика BMP280
* Напряжение питания: 1.71V – 3.6V;
* Интерфейс обмена данными: I2C или SPI;
* Ток потребления в рабочем режиме: 2.7uA при частоте опроса 1 Гц;
* Диапазон измерения атмосферного давления: 300hPa – 1100hPa (±0.12hPa), что эквивалентно диапазону от -500 до 9000 м над уровнем моря;
* Диапазон измерения температуры: -40°С … +85°С (±0.01°С);
* Максимальная частота работы интерфейса I2C: 3.4MHz;
* Максимальная частота работы интерфейса SPI: 10 МГц;
* Размер модуля: 21 х 18 мм.


## Нам понадобятся

1. Датчик  DH11, измеряющий температуру и влажность, либо датчик BMP280, измеряющий температуру, давление и высоту.
2. Экран: Matrix 8x8 4шт. (бегущая строка), либо экран LCD1602, либо сегментный цифровой индикатор для вывода показателей.
3. Соединительные провода, перемычки (для соединения матричных экранов).

Соберем схему на контроллере Arduino Uno, SD картой, датчиком  DH11 и дисплеем LCD1602, для подключения используем данную таблицу

## Pin Wiring

|Arduino|DH11|BMP280|LCD1602|LED matrix|сегментный индикатор|SD|RTC|
|-------|----|------|-------|----------|--------------------|--|---|
|5V|VCC|5V|5V|5V|5V|5V | 5V| 
|GND|GND|GND|GND|GND|GND| GND| GND|
|13| |SCK| |SCL| |CLK| |
|12| |SDO| | | |DO| |
|11| |SDI| |SDI| |DI| |
|9| | | | | |CS| |
|8|OUT|CS | | | | | |
|7| | | | |DIO| | |
|6| | | | |CLK| | |
|5| | | |CS| | | |
|4| | | | | | | || | | | | |
|A4| | | SCL| | | | | |SDA |
|A5| | |SDA| | | |SCL |


Для создания бегущей строки, матричные экраны собираются последовательно, перемычками (контакт SDO соединяется с контактом SDI ). Для удобства использования экраны можно склеить.

## Схемы подключений датчика и различных экранов

![](Tutorials/meteo_ard_sch_lcd.jpg)
![](Tutorials/meteo_ard_sch_matrix.jpg)
![](Tutorials/meteo_ard_sch_segm.jpg)

## Библиотеки

В зависимости от используемых модулей установите соответствующие библиотеки.

  RTClib.h библиотека часов реального времени  
  LiquidCrystal_I2C.h библиотека дисплея LCD1602  
  Adafruit_BMP280.h библиотека датчика BMP280  
  Adafruit_GFX.h библиотека вычислений GFX  
  Max72xxPanel.h библиотека матрицы Бегущая строка  
  TM1637.h библиотека сегментного индикатора  
  DHT.h библиотека датчика DH11  
  SPI.h библиотека шины SPI для работы с SD  
  SD.h библиотека рабооты с SD картой  


## Настройки Arduino IDE для платы Arduino UNO 

![](Tutorials/meteo_ard_ide.png)

## Код

```cpp

#include "RTClib.h" //библиотека часов реального времени
#include <LiquidCrystal_I2C.h>  //библиотека дисплея LCD1602
#include <Adafruit_BMP280.h>  //библиотека датчика BMP280
#include <Adafruit_GFX.h> //библиотека вычислений GFX
#include "TM1637.h" //библиотека сегментного индикатора
#include "DHT.h"  //библиотека датчика DH11
#include <SPI.h>  //библиотека шины SPI для работы с SD
#include <SD.h> //библиотека работы с SD картой
#include <Max72xxPanel.h> //библиотека матрицы Бегущая строка

//#define useSD
#define useDH11
//#define useBMP280
#define useLCD1602
//#define useTICKER   //бегущая строка
//#define useSEGMSCR  //сегментный индикатор

#define addrLCD       0x38  //адрес дисплея LCD1602
#define dht11PIN      8    //пин датчика DH11
#define sdPIN         9       //пин SD
#define csMtrxPIN     5       //пин Matrix CS
#define countTicker   4       //количество матриц Бегущая строка

String paramStr;
float temperature;
float pressure;
float altitude;
float humidity;

#ifdef useDH11
  DHT dht11(dht11PIN, DHT11);
#endif

#ifdef useBMP280
  #define BMP_SCK  13
  #define BMP_MISO 12
  #define BMP_MOSI 11
  #define BMP_CS   8
  Adafruit_BMP280 bmp280(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);
#endif

#ifdef useLCD1602
  LiquidCrystal_I2C lcd(addrLCD,16,2);
#endif

#ifdef useSEGMSCR
  TM1637 tm1637(6,7);
#endif

#ifdef useTICKER
  Max72xxPanel matrix = Max72xxPanel(csMtrxPIN, countTicker, 1);
#endif

RTC_DS1307 RTC;

void setup(){
  Serial.begin(115200);
  
  Wire.begin(); //инициализируем шину I2C

  #ifdef useLCD1602 //инициализируем LCD1602
    lcd.init();
    lcd.clear();
    lcd.backlight();
  #endif

  #ifdef useSEGMSCR
    tm1637.init();
    tm1637.set(BRIGHT_TYPICAL);
  #endif

  #ifdef useBMP280   //инициализируем BMP280
    if(!bmp280.begin()){
      Serial.println("BMP280 Error");
    }
  #endif
  
  #ifdef useDH11   //инициализируем DH11
    dht11.begin();
  #endif

  #ifdef useSD
  if(!SD.begin(sdPIN)){
    Serial.println("SD Error");
    Serial.println("Break start");while(1){}
  }
  #endif

  RTC.begin();
  //RTC.adjust(DateTime(__DATE__, __TIME__)); //установить время компьютера
  
  if(!RTC.isrunning()){
    Serial.println("RTC Error");
    Serial.println("Break start");while(1){}
  }
  Serial.println("Std");
}
 
void loop(){
  DateTime now = RTC.now();
  
  #ifdef useDH11
    temperature = dht11.readTemperature();
    humidity = dht11.readHumidity();
    #ifdef useLCD1602
      lcd.setCursor(5,0);
      if(now.hour() < 10){lcd.print("0");}lcd.print(now.hour());lcd.print(":");
      if(now.minute() < 10){lcd.print("0");}lcd.print(now.minute());
      lcd.setCursor(0,1);lcd.print("T=");lcd.print(temperature) + "C";
      lcd.setCursor(9,1);lcd.print("H=");lcd.print(humidity);
    #endif
    #ifdef useTICKER
      String mxStr = "";
      if(now.hour() < 10){mxStr += "0";}mxStr += String(now.hour()) + ":";
      if(now.minute() < 10){mxStr += "0";}mxStr += String(now.minute());
      mxStr += " t=" + String(int(temperature)) + "C  H=" + String(int(humidity)) + "%";
      matrixPrint(mxStr);
    #endif
    #ifdef useSEGMSCR
      String dTemp = String((int)temperature);
      tm1637.display(0, dTemp[0]-48);
      tm1637.display(1, dTemp[1]-48);
      tm1637.display(3, 12);
    #endif
    
  #endif

  #ifdef useBMP280
    temperature = bmp280.readTemperature();
    pressure = bmp280.readPressure()*0.00750063755419211;
    altitude = bmp280.readAltitude(1013.25);
    #ifdef useLCD1602
      lcd.setCursor(5,0);
      if(now.hour() < 10){lcd.print("0");}lcd.print(now.hour());lcd.print(":");
      if(now.minute() < 10){lcd.print("0");}lcd.print(now.minute());
      lcd.setCursor(0,1);lcd.print("T=");lcd.print(temperature) + "C";
      lcd.setCursor(9,1);lcd.print("P=");lcd.print(pressure);
    #endif
    #ifdef useTICKER
      String mxStr = "";
      if(now.hour() < 10){mxStr += "0";}mxStr += String(now.hour()) + ":";
      if(now.minute() < 10){mxStr += "0";}mxStr += String(now.minute());
      mxStr += " t=" + String(int(temperature)) + "C P=" + String(int(pressure)) + " A=" + String(int(altitude));
      matrixPrint(mxStr);
    #endif
    #ifdef useSEGMSCR
      String dTemp = String((int)temperature);
      tm1637.display(0, dTemp[0]-48);
      tm1637.display(1, dTemp[1]-48);
      tm1637.display(3, 12);
    #endif
  #endif

  paramStr = "";
  paramStr += String(now.hour());paramStr += ":";
  paramStr += String(now.minute());paramStr += ":";
  paramStr += String(now.second());
  paramStr += " T:";paramStr += String(temperature);
  paramStr += " P:";paramStr += String(pressure);
  paramStr += " A:";paramStr += String(altitude);
  paramStr += " H:";paramStr += String(humidity);
  
  #ifdef useSD
  File dataFile = SD.open("file.txt", FILE_WRITE);
  dataFile.println(paramStr);
  dataFile.close();
  #endif

  Serial.println(paramStr);

}

void matrixPrint(String tape){
  #ifdef useTICKER
  int spacer = 1;
  int width = 5 + spacer;
  for ( int i = 0 ; i < width * tape.length() + matrix.width() - 1 - spacer; i++ ) {

    matrix.fillScreen(LOW);

    int letter = i / width;
    int x = (matrix.width() - 1) - i % width;
    int y = (matrix.height() - 8) / 2; // center the text vertically

    while ( x + width - spacer >= 0 && letter >= 0 ) {
      if ( letter < tape.length() ) {
        matrix.drawChar(x, y, tape[letter], HIGH, LOW, 1);
      }

      letter--;
      x -= width;
    }

    matrix.write(); // Send bitmap to display

    delay(30);
  }
  #endif
}


```

## DH11 + LCD1602 + SD + RTC

![](Tutorials/meteo_ard_lcd.gif)


## DH11 + LED matrix + SD + RTC

![](Tutorials/meteo_ard_matrix.gif)


## DH11 + сегментный индикатор + SD + RTC

![](Tutorials/meteo_ard_segm.gif)

## На SD карту в файл file.txt пишутся данные с датчика в таком виде:

10:04:07 T:34.73 P:734.59 A:376.27 H:0.00  
10:04:09 T:34.73 P:734.59 A:376.27 H:0.00  
10:04:10 T:34.73 P:734.59 A:376.27 H:0.00  


